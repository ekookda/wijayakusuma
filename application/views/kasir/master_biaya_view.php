	<!-- Page Content -->
	<div id="page-wrapper">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">Master Biaya Pendidikan</h1>
				</div>
				<!-- /.col-lg-12 -->
			</div>

			<?= form_open('petugas/biaya/master-biaya', ['class'=>'form-inline']); ?>

				<div class="form-group">

					<select name="pilih_jenjang" id="pilih_jenjang" class="form-control" required>
						<option value="">-- Pilih Jenjang --</option>
						<?php
						$qry = $this->db->get('tbl_pendidikan')->result();
						foreach ($qry as $list)
						{
							echo "<option value='".$list->id_pendidikan."'>".strtoupper($list->jenjang)."</option>";
						}
						?>
					</select>

					<input type="submit" name="btn-pilih" value="Tampilkan" class="btn btn-primary">

				</div>

			<?= form_close(); ?>
			
			<br>

			<div class="row" id="datatables" style="display:none">
				<!-- Datatables Level Pendidikan -->
				<div class="col-lg-12 col-sm-12">
					<div class="panel panel-info">
						<!-- panel-heading -->
						<div class="panel-heading">
							<i class="fa fa-info-circle"></i> Master Biaya Pendidikan <span id="nama_jenjang"></span>
						</div>
						<!-- panel-heading -->

						<!-- panel-body -->
						<div class="panel-body">
							<div class="row">

								<!-- Button Tambah -->
								<div class="col-lg-6 col-sm-4 col-xs-4">
									<button type="button" class="btn btn-primary btn-flat" data-toggle="modal" data-target="#form-tambah">
										<i class="fa fa-plus-circle"></i> Tambah Biaya
									</buton>
								</div>

							</div>
							<br>

							<div class="table-responsive">
								<table id="tabel_jenjang" class="table display table-hover table-striped table-condensed table-bordered" style="width:100%">
									<thead>
										<tr>
											<th class="text-center">#</th>
											<th class="text-center">Kode</th>
											<th class="text-center">Nama Biaya</th>
											<th class="text-center">Jumlah Biaya</th>
											<th class="text-center">Action</th>
										</tr>
									</thead>

									<tbody class="text-center" id="tbody">
										<?php
										if ($_jenjang != NULL)
										{
											$no = 1;
											foreach ($_jenjang as $r)
											{
												echo "<tr>";
												echo "<td>".$no++."</td>";
												echo "<td>$r->id_biaya</td>";
												echo "<td>$r->nama_biaya</td>";
												echo "<td> Rp ".number_format($r->jml_biaya, 0, '', '.')."</td>";
												echo "<td>";
													echo "<a href='".base_url()."petugas/biaya/edit/".$r->id_biaya."'>Edit</a> | <a href='".base_url()."petugas/biaya/hapus/".$r->id_biaya."'>Hapus</a>";
												echo "</td>";
												echo "</tr>";
											}
										}
										?>
									</tbody>
								</table>
							</div>
							<!-- /.table-responsive -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-5 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</div>
	<!-- #page-wrapper -->
<script type="text/javascript">
$(document).ready(function ()
{
	var id = "<?=$_id;?>";
	var tingkat;

	if (id != '')
	{
		switch (id)
		{
			case '01':
				tingkat = 'TK';
				break;
		
			case '02':
				tingkat = 'SD';
				break;

			case '03':
				tingkat = 'SMP';
				break;

			case '04':
				tingkat = 'SMA';
				break;

			case '05':
				tingkat = 'SMK';
				break;

			default:
				tingkat = '';
				break;
		}

		$('#nama_jenjang').html("Tingkat " + tingkat);

		$('#datatables').show(function () {
			$('#tabel_jenjang').dataTable();
		});
	}

	<?php // if ($_jenjang != NULL): ?>
	// 	$('#datatables').show(function () {
	// 		$('#tabel_jenjang').dataTable();
	// 	});
	// <?php // else: ?>
	// 	$('#datatables').show(function () {
	// 		$('#tabel_jenjang').dataTable();
	// 	});
	// <?php //endif; ?>

	// notify
	<?php if ($this->session->flashdata('error')) : ?>
		$.notify({
			icon: 'glyphicon glyphicon-warning-sign',
			message: '<?= $this->session->flashdata('error'); ?>',
		}, {
			type: 'danger'
		});
	<?php endif; ?>

});
</script>

<!-- // END CONTENT -->

<!-- MODAL -->

<!-- Form Modal -->
<div id="form-tambah" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Form Tambah Biaya</h4>
			</div>
			
			<?= form_open('kasir/biaya/set_biaya', ['id'=>'tambah_biaya', 'class'=>'form-horizontal']); ?>

			<div class="modal-body">
				<div class="form-group">
					<label class="control-label col-sm-4" for="jenjang">Jenjang</label>
					<div class="col-sm-8">
						<select name="jenjang" id="jenjang" class="form-control">
							<option value="">-- Pilih Jenjang --</option>
							<?php
							$jenjang = array(
								'01' => 'TK',
								'02' => 'SD',
								'03' => 'SMP',
								'04' => 'SMA',
								'05' => 'SMK' 
							);
							foreach ($jenjang as $val => $list) {
								echo "<option value='".$val."'>".$list."</option>";
							}
							?>
						</select>
					</div>
				</div>
				
				<div class="form-group">
					<label class="control-label col-sm-4" for="kd_biaya">Kode Biaya</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="kd_biaya" name="kd_biaya" disabled>
					</div>
				</div>
				<script type="text/javascript">
					$('#jenjang').change(function () {
						var id_jenjang = $(this).val();
						$.ajax({
							type: "post",
							url: "<?= site_url('kasir/kd_biaya'); ?>",
							data: "pendidikan_id=" + id_jenjang,
							dataType: "json",
							success: function (data) {
								$('#kd_biaya').val(data);
								// console.log(data);
							}
						});
					});
				</script>

				<div class="form-group">
					<label class="control-label col-sm-4" for="nm_biaya">Nama Biaya</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="nm_biaya" value="">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-4" for="jml_biaya">Jumlah Biaya</label>
					<div class="col-sm-8">
						<input type="number" class="form-control" id="jml_biaya" value="">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="btn-simpan">Simpan</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>

			<?= form_close(); ?>

		</div>

	</div>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#btn-simpan').click(function () {
		var url = $('#tambah_biaya').attr('action');
		// console.log(url);
		var id_jenjang, id_biaya, nm_biaya, jml_biaya;

		id_jenjang = $('#jenjang').val();
	});
});
</script>

<!-- END MODAL -->
