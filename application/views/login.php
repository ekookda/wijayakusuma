<!DOCTYPE html>
<html lang="en">
<head>
	<title>{title}</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>vendor/twbs/bootstrap/dist/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/main.css">
<!--===============================================================================================-->
</head>
<body style="background-color: #999999;">
	
	<div class="limiter">
		<div class="container-login100">
			<div class="login100-more" style="background-image: url('<?=base_url();?>assets/images/bg-02.jpg');"></div>

			<div class="wrap-login100 p-l-50 p-r-50 p-t-72 p-b-50">

				<!-- Form Open -->
				<?= form_open('login/validation', array('class'=>'login100-form validate-form')); ?>
				
					<!-- Title -->
					<span class="login100-form-title p-b-59"><i class="fa fa-adn"></i> APEM LOGIN</span>

					<!-- Form Input -->
					<div class="wrap-input100 validate-input" data-validate="Username is required">
						<span class="label-input100">Username</span>
						<p class="text-danger"><?=form_error('username');?></p>
						<?= form_input('username', set_value('username'), array('class'=>'input100', 'placeholder'=>'Username...')); ?>
						<span class="focus-input100"></span>
					</div>

					<!-- Form Password -->
					<div class="wrap-input100 validate-input" data-validate = "Password is required">
						<span class="label-input100">Password</span>
						<p class="text-danger"><?=form_error('password');?></p>
						<?= form_password('password', set_value('password'), array('class'=>'input100', 'placeholder'=>'*************')); ?>
						<span class="focus-input100"></span>
					</div>

					<!-- Button Login -->
					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button type="submit" class="login100-form-btn" name="btn-login">Login</button>
						</div>
					</div>

				<?= form_close(); ?>

			</div>
		</div>
	</div>

<!--===============================================================================================-->
	<script src="<?=base_url();?>vendor/components/jquery/jquery.min.js"></script>
<!--===============================================================================================-->
	<script src="<?=base_url();?>assets/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="<?=base_url();?>assets/vendor/bootstrap/js/popper.js"></script>
	<script src="<?=base_url();?>vendor/twbs/bootstrap/dist/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?=base_url();?>assets/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="<?=base_url();?>assets/js/main.js"></script>

	<script>
		<?php if ($this->session->flashdata('error')): ?>
			alert("<?=$this->session->flashdata('error');?>");
		<?php endif; ?>
	</script>
</body>
</html>
