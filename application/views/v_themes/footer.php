<!-- jQuery -->
<script src="<?= base_url(); ?>assets/template/sb-admin/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?= base_url(); ?>assets/template/sb-admin/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Datatables JavaScript -->
<script src="<?= base_url(); ?>vendor/datatables/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url(); ?>vendor/datatables/datatables/media/js/dataTables.bootstrap.min.js"></script>

<!-- Bootstrap Notify  -->
<script src="<?= base_url(); ?>vendor/bootstrap-notify/bootstrap-notify.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?= base_url(); ?>assets/template/sb-admin/vendor/metisMenu/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<?= base_url(); ?>assets/template/sb-admin/dist/js/sb-admin-2.js"></script>
<script src="<?= base_url(); ?>assets/js/mycustom.js"></script>
