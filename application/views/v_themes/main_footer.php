<div class="footer">
	<div class="row">
		<div class="col-lg-12">
			&copy; <?= date('Y'); ?> eko.okda | Design by:
			<a href="http://binarytheme.com" style="color:#fff;" target="_blank">www.binarytheme.com</a>
		</div>
	</div> <!-- /.row -->
</div> <!-- /.footer -->
