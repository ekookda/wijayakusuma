	<div class="navbar-default sidebar" role="navigation">
		<div class="sidebar-nav navbar-collapse">
			<ul class="nav" id="side-menu">
				<?php
				$levels = $this->session->userdata('level');

				if ($levels == 'admin') :
				?>
					<!-- Beranda -->
					<li>
						<a href="<?= site_url('admin/'); ?>"><i class="fa fa-home fa-fw"></i> Beranda</a>
					</li>

					<!-- Organisasi -->
					<li>
						<a href="#"><i class="fa fa-building fa-fw"></i> Sekolah<span class="fa arrow"></span></a>
						<ul>
							<li>
								<a href="<?= site_url('admin/profil'); ?>"><i class="fa fa-address-card"></i> Profil Sekolah</a>
							</li>
							<li>
								<a href="<?= site_url('admin/jenjang'); ?>"><i class="fa fa-address-card"></i> Tingkat Pendidikan</a>
							</li>
						</ul>
					</li>

					<!-- Pembayaran -->
					<li>
						<a href="<?= site_url('admin/pembayaran'); ?>"><i class="fa fa-calculator fa-fw"></i> Pembayaran</a>
					</li>

				<?php elseif($levels == 'petugas') : ?>

					<!-- Beranda -->
					<li>
						<a href="<?= site_url('kasir'); ?>"><i class="fa fa-home fa-fw"></i> Beranda</a>
					</li>
					<!-- Pembayaran -->
					<li>
						<a href="#"><i class="fa fa-paypal fa-fw"></i> Master Biaya<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
							<li>
								<a href="<?= site_url('petugas/biaya/master-biaya'); ?>"><i class="fa fa-paypal fa-fw"></i> Master Biaya<span class=""></span></a>
							</li>
						</ul>
					</li>

				<?php elseif($levels == 'direktur') : ?>

				<?php else : ?>

				<?php endif; ?>

			</ul>
		</div>
		<!-- /.sidebar-collapse -->
	</div>
	<!-- /.navbar-static-side -->
</nav>
