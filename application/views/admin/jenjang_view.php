	<!-- Page Content -->
	<div id="page-wrapper">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">Tingkat Pendidikan</h1>
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->

			<!-- Form -->
			<div class="row">
				<!-- Datatables Level Pendidikan -->
				<div class="col-lg-12 col-sm-12">
					<div class="panel panel-info">
						<!-- panel-heading -->
						<div class="panel-heading">
							<i class="fa fa-info-circle"></i> Datatables Tingkat Pendidikan
						</div>
						<!-- panel-heading -->

						<!-- panel-body -->
						<div class="panel-body">
							
							<div class="table-responsive">
								<div class="form-group">
									<a href="<?=site_url('admin/jenjang/tambah');?>" class="btn btn-primary btn-flat">
										<i class="fa fa-plus-circle"></i> Add</a>
								</div>

								<table id="tabel_jenjang" class="table display table-hover table-striped table-condensed table-bordered" style="width:100%">
									<thead>
										<tr>
											<th class="text-center">#</th>
											<th class="text-center">ID</th>
											<th class="text-center">Tingkat</th>
											<th class="text-center">Action</th>
										</tr>
									</thead>

									<tbody class="text-center">
									<?php $no = 1; foreach ($data->result() as $r) : ?>
										<tr>
											<td><?=$no++;?></td>
											<td><?=$r->id_pendidikan;?></td>
											<td><?=$r->jenjang?></td>
											<td>
												<a href="#">Edit</a> | 
												<a href="#">Delete</a>
											</td>
										</tr>
									<?php endforeach; ?>
									</tbody>
								</table>
							</div>
							<!-- /.table-responsive -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-5 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</div>
	<!-- #page-wrapper -->
<script type="text/javascript">
$(document).ready(function () {

	$('#tabel_jenjang').dataTable();

	// notify
	<?php if ($this->session->flashdata('error')) : ?>
		$.notify({
			icon: 'glyphicon glyphicon-warning-sign',
			message: '<?= $this->session->flashdata('error'); ?>',
		}, {
			type: 'danger'
		});
	<?php endif; ?>

});
</script>
