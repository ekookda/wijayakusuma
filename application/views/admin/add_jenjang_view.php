	<!-- Page Content -->
	<div id="page-wrapper">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">Tingkat Pendidikan</h1>
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->

			<!-- Form -->
			<div class="row">
				<div class="col-lg-12 col-sm-12">
					<div class="panel panel-default">
						<!-- panel-heading -->
						<div class="panel-heading">
							<i class="fa fa-plus-circle"></i> Form Tambah Jenjang
						</div>
						<!-- panel-heading -->

						<!-- panel-body -->
						<div class="panel-body">
							<?php echo form_open('admin/jenjang/simpan', ['id'=>'form_pendidikan', 'role'=>'form', 'class'=>'form-horizontal'], ['id_pendidikan'=>$max_id]); ?>

							<div class="form-group">
								<label for="id" class="control-label col-sm-2">ID</label>
								<div class="col-sm-6">
									<input type="text" name="id_pendidikan" id="id_pendidikan" class="form-control" minlength="2" maxlength="3" value="<?=$max_id;?>" disabled>
									<?= form_error('id_pendidikan', '<div class="alert alert-danger">', '</div>'); ?>
								</div>
							</div>

							<div class="form-group">
								<label for="id" class="control-label col-sm-2">Level</label>
								<div class="col-sm-6">
									<?php
									$options = [
										'' => '-- pilih jenjang --',
										'TK' => 'TK',
										'SD' => 'SD',
										'SMP' => 'SMP',
										'SMA' => 'SMA',
										'SMK' => 'SMK'
									];
									echo form_dropdown('lvl_pendidikan', $options, '', ['id' => 'lvl_pendidikan', 'class' => 'form-control']);
									
									echo form_error('lvl_pendidikan', '<div class="text-danger">', '</div>'); 
									?>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-offset-2 col-sm-6">
									<button id='tambah_pendidikan' class="btn btn-primary" type="submit" name="tambah_pendidikan"><span id="loading" style="display:none"><i class="fa fa-spinner fa-spin"></i></span> Simpan</button>
								</div>
							</div>

							<?= form_close(); ?>
						</div>
						<!-- /.panel-body -->
					</div>
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</div>
	<!-- /#page-wrapper -->

<script type="text/javascript">
$(document).ready(function () {
	// notify
	<?php if ($this->session->flashdata('error')) : ?>
		$.notify({
			icon: 'glyphicon glyphicon-warning-sign',
			message: '<?= $this->session->flashdata('error'); ?>',
		}, {
			type: 'danger'
		});
	<?php endif; ?>
});
</script>
