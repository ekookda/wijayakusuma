<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Crud_m extends CI_Model 
{
	public function get_order_by($table, $sort)
	{
		$this->db->order_by($sort);
		$hasil = $this->db->get($table);

		return $hasil;
	}

	public function read($table)
	{
		$hasil = $this->db->get($table);

		return $hasil;
	}

	public function read_by($table, $where = array())
	{
		$hasil = $this->db->get_where($table, $where);

		return $hasil;
	}

	public function store($table, $data=array())
	{
		$this->db->insert($table, $data);

		return $this->db->insert_id();
	}

	public function update()
	{
		
	}

	public function drop()
	{
		
	}

/*
	public function get_all_biaya($id)
	{
		$this->datatables->select('id_biaya, nama_biaya, jml_biaya, pendidikan_id');
		$this->datatables->from('tbl_pendidikan');
		$this->datatables->join('tbl_biaya', 'tbl_biaya.pendidikan_id = tbl_pendidikan.id_pendidikan');
		$this->datatables->where(['pendidikan_id' => $id]);
		// $this->datatables->order_by('pendidikan_id ASC, id_biaya ASC');
		$this->datatables->add_column('view', '<a href="'.site_url('petugas/biaya/edit/').'$1">edit</a> | <a href="'.site_url('petugas/biaya/delete/').'$1">delete</a>', 'id_biaya');
		
		return $this->datatables->generate();
	}
*/

	public function get_biaya($id)
	{
		$this->db->select('id_biaya, nama_biaya, jml_biaya, pendidikan_id, jenjang');
		$this->db->from('tbl_pendidikan');
		$this->db->join('tbl_biaya', 'tbl_biaya.pendidikan_id = tbl_pendidikan.id_pendidikan');
		$this->db->where(['pendidikan_id' => $id]);
		// $this->datatables->order_by('pendidikan_id ASC, id_biaya ASC');
		// $this->datatables->add_column('view', '<a href="'.site_url('petugas/biaya/edit/').'$1">edit</a> | <a href="'.site_url('petugas/biaya/delete/').'$1">delete</a>', 'id_biaya');
		
		return $this->db->get();
	}

}

/* End of file Crud_m.php */
