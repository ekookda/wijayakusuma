<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Login_m extends CI_Model
{
	
	private $table = "tbl_petugas";

	public function checkLogin($where, $limit=1)
	{
		$get = $this->db->get_where($this->table, $where, $limit);

		return $get;
	}

	public function lastActivity($where, $data)
	{
		$this->db->where($where);
		$update = $this->db->update($this->table, $data);

		return $update;
	}

}

/* End of file Login_m.php */
