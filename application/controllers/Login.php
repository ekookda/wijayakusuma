<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{

	private $required = "%s wajib diisi";

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('security');
		$this->load->model('Login_m');
	}
	

	public function index()
	{
		$data = array(
			'title' => 'Aplikasi Pembayaran Sekolah',
		);

		$this->parser->parse('login', $data);
	}

	public function validation()
	{
		if (!isset($_POST['btn-login']))
		{
			// return false
			$this->session->set_flashdata('error', 'anda belum melakukan login!');

			$this->index();
		}
		else
		{
			// return true
			$this->form_validation->set_rules('username', 'Username', 'trim|required', array('required' => $this->required));
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]', array('required' => $this->required, 'min_length'=>'%s minimal 8 karakter'));

			if ($this->form_validation->run() == FALSE)
			{
				// validasi form gagal
				$this->index();
			} 
			else
			{
				$username = $this->security->xss_clean($this->input->post('username', TRUE));
				$password = $this->security->xss_clean($this->input->post('password', TRUE));
				
				$where = array('username' => $username);

				$get_m = $this->Login_m->checkLogin($where);

				if ($get_m->num_rows() < 1)
				{
					$this->session->set_flashdata('error', 'Data tidak ditemukan!');
					$this->index();
				}
				else
				{
					$result = $get_m->result();
					$password_verify = password_verify($password, $result[0]->password);

					if ($password_verify === TRUE)
					{
						// username dan password cocok
						// Update last activity
						$where = array('id_petugas' => $result[0]->id_petugas);
						$data  = array('last_activity' => date('Y-m-d H:i:s'));
						$updateLastActivity = $this->Login_m->lastActivity($where, $data);

						// set session userdata
						$session_data = array(
							'id_petugas' => $result[0]->id_petugas,
							'level' => $result[0]->level,
							'isLoggedIn' => TRUE
						);
						$this->session->set_userdata($session_data);

						$level = $result[0]->level;

						switch ($level) {
							case 'admin':
								$redirect = 'admin';
								break;

							case 'petugas':
								$redirect = 'kasir';
								break;

							default:
								$redirect = 'login';
								break;
						}
						redirect($redirect);
					}
					else
					{
						// password tidak cocok
						// passing ke halaman login
						$this->session->set_flashdata('error', 'Password tidak cocok!');
						$this->index();
					}
				}
			}
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();

		redirect('login');
	}

}

/* End of file Login.php */
