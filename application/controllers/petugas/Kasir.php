<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Kasir extends CI_Controller 
{

	private $main = 'v_themes/main';
	public $id_biaya_jenjang;

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('isLoggedIn') != TRUE || $this->session->userdata('level') != 'petugas')
		{
			$this->session->set_flashdata('error', 'Silahkan Login Kembali!');

			redirect('login');
		}

		$this->load->model('crud_m');
		
	}

	public function index()
	{
		$data = array(
			'title' => 'Page Kasir',
			'_content' => 'kasir/input_pembayaran'
		);

		$this->load->view($this->main, $data);
	}

	public function profil($id)
	{
		
	}

	public function master_biaya()
	{
		$data = array(
			'title' => 'Data Master Biaya', 
			'_content' => 'kasir/master_biaya_view',
			'jenjang' => $this->get_biaya()
		);

		$this->load->view($this->main, $data);
	}

	public function get_biaya()
	{
		$this->db->select('*');
		$this->db->from('tbl_pendidikan');
		$this->db->join('tbl_biaya', 'tbl_biaya.pendidikan_id = tbl_pendidikan.id_pendidikan');
		$this->db->order_by('pendidikan_id ASC, id_biaya ASC');
		$jenjang = $this->db->get();
		
		// $jenjang = $this->crud_m->get_order_by('tbl_biaya', 'pendidikan_id ASC, id_biaya ASC')->result();
		
		return $jenjang->result();
	}

	public function kd_biaya()
	{
		$id = $this->input->post('pendidikan_id');

		$this->db->where(['pendidikan_id' => $id]);
		$this->db->select_max('id_biaya');
		$hasil = $this->db->get('tbl_biaya')->result_array();
		$id_biaya = $hasil[0]['id_biaya'];

		if ($id_biaya != null)
		{
			// kalau hasilnya ada, pisahkan masing2 kode
			$depan = substr($id_biaya, 0, 2);
			$belakang = substr($id_biaya, -2);
			$tambah = (int) $belakang + 1;

			if ($tambah < 10)
			{
				$r = str_pad($depan, 4, "0".$tambah, STR_PAD_RIGHT);
			}
			else
			{
				$r = str_pad($depan, 4, $tambah, STR_PAD_RIGHT);
			}
		}
		else
		{
			$r = str_pad($id, 4, "01", STR_PAD_RIGHT);
		}

		echo json_encode($r);
	}

	public function set_biaya()
	{
		$this->id_biaya_jenjang = $this->input->post('id_jenjang');

		echo $this->master_biaya_jenjang($this->id_biaya_jenjang);
	}

}

/* End of file Kasir.php */
