<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Jenjang extends CI_Controller 
{
	private $v_themes = 'v_themes/main';

	public function __construct()
	{
		parent::__construct();

		$this->load->model('crud_m');

		if ($this->session->userdata('isLoggedIn') != TRUE || $this->session->userdata('level') != 'admin')
		{
			$this->session->set_flashdata('error', 'Silahkan Login Kembali!');

			redirect('login');
		}
	}

	public function index()
	{
		$this->data = [
			'title' => 'Pages Admin',
			'_content' => 'admin/jenjang_view',
			'data' => $this->crud_m->read('tbl_pendidikan')
		];

		$this->load->view($this->v_themes, $this->data);
	}

	public function read()
	{
		$data = $this->crud_m->read('tbl_pendidikan');

		echo json_encode($data);
	}

	public function tambah()
	{
		$this->db->select_max('id_pendidikan');
		$qry = $this->crud_m->read('tbl_pendidikan');

		$row = $qry->row_array();
		$last_id = $row['id_pendidikan'];
		$last_id = (int) $last_id;

		if ($last_id == 0)
		{
			$last_id = 1; 
		}
		else
		{
			$last_id++;
		}
		$max_id = str_pad($last_id, 2, "0", STR_PAD_LEFT);

		$data = [
			'title' => 'Tambah Jenjang Pendidikan',
			'_content' => 'admin/add_jenjang_view',
			'max_id' => $max_id
		];

		$this->load->view($this->v_themes, $data);
	}

	public function simpan()
	{
		if(isset($_POST['tambah_pendidikan']))
		{
			$this->form_validation->set_rules('id_pendidikan', 'ID', 'trim|required|min_length[2]|max_length[2]');
			$this->form_validation->set_rules('lvl_pendidikan', 'Jenjang '.$this->input->post('lvl_pendidikan'), 'trim|required|is_unique[tbl_pendidikan.jenjang]',
				[
					'required' => 'Form wajib diisi',
					'is_unique' => '%s sudah pernah disimpan'
				]
			);
			
			if ($this->form_validation->run() == FALSE) 
			{
				$this->tambah();
			}
			else
			{
				$data = [
					'id_pendidikan' => $this->input->post('id_pendidikan', TRUE),
					'jenjang' => $this->input->post('lvl_pendidikan', TRUE)
				];

				$this->crud_m->store('tbl_pendidikan', $data);

				redirect('admin/jenjang');
			}
			return FALSE;
		}
		else
		{
			$this->tambah();
		}
	}

}

/* End of file Jenjang.php */
