<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller 
{
	private $v_themes = 'v_themes/main';

	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('isLoggedIn') != TRUE || $this->session->userdata('level') != 'admin')
		{
			$this->session->set_flashdata('error', 'Silahkan Login Kembali!');

			redirect('login');
		}
	}

	public function index()
	{
		$this->data = [
			'title' => 'Pages Admin',
			'_content' => 'admin/index'
		];

		$this->load->view($this->v_themes, $this->data);
	}


}

/* End of file Admin.php */
